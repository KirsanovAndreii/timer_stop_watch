package com.ankir33gmail.stopwatchandtabbat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by AnKir on 25.04.2017.
 */

public class StopWatchFragment extends Fragment {
    private static final String KEY = "key";
    private static final String KEY_StartTime = "keyST";
    private int startTime = 0;

    public static StopWatchFragment newInstance(String nameFragment) {

        Bundle args = new Bundle();
        args.putString(KEY, nameFragment);
        StopWatchFragment fragment = new StopWatchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    String value;

    @BindView(R.id.textStopW)
    TextView textStopW;
    @BindView(R.id.textCounter)
    TextView textCounter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(KEY)) {
            value = getArguments().getString(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        //тот же findById для text. Биндим вьюхи
        ButterKnife.bind(this, root);
        textStopW.setText(value);
        if (savedInstanceState != null) {startTime = Integer.parseInt(savedInstanceState.getString(KEY_StartTime));
            textCounter.setText(savedInstanceState.getString(KEY_StartTime));}
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      //  if (savedInstanceState != null) textCounter.setText(savedInstanceState.getString(KEY_StartTime));
    }

    CounterProzess mCounterProzess;
    boolean onStart = false;


    @OnClick(R.id.textCounter)
    void onClickCounter() {

        if (!onStart) {
            mCounterProzess = new CounterProzess();
            mCounterProzess.execute(startTime);
            onStart = true;

        } else {
            mCounterProzess.cancel(true);
            startTime = Integer.parseInt(textCounter.getText().toString());
            onStart = false;
        }
    }

    @OnLongClick(R.id.textCounter)
    boolean onClickLongCounter() {
        mCounterProzess.cancel(true);
        startTime = 0;
        onStart = false;
        textCounter.setText("0");
        return true;
    }


    class CounterProzess extends AsyncTask<Integer, Integer, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            int timeStart = params[0];
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                timeStart++;
                publishProgress(timeStart);
                if (isCancelled()) return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            textCounter.setText(values[0] + "");
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_StartTime, textCounter.getText().toString());
    }
}
