package com.ankir33gmail.stopwatchandtabbat;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;


/**
 * Created by AnKir on 25.04.2017.
 */

public class TimerFragment extends Fragment {

    private static final String KEY = "key";

    public static TimerFragment newInstance(String nameFragment) {

        Bundle args = new Bundle();
        args.putString(KEY, nameFragment);
        TimerFragment fragment = new TimerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    String value;

    @BindView(R.id.textTimer)
    TextView textTimer;
    @BindView(R.id.textCounterT)
    TextView textCounterT;
    @BindView(R.id.iter)
    TextView textIter;
    @BindView(R.id.textWorkRelax)
    TextView textWorkRelax;
    @BindView(R.id.fon)
    RelativeLayout fon;
  /*  @BindView(R.id.work)
    TextView work;
    @BindView(R.id.relax)
    TextView relax;
    @BindView(R.id.iterac)
    TextView iterac;*/
    @BindView(R.id.workV)
    TextView workV;
    @BindView(R.id.relaxV)
    TextView relaxV;
    @BindView(R.id.iteracV)
    TextView iteracV;


    private int totalIter = 3; //входные изменяемые параметры
    private int timeWork = 10;
    private int timeRelax = 5;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(KEY)) {
            value = getArguments().getString(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_timer, container, false);
//тот же findById для text. Биндим вьюхи
        ButterKnife.bind(this, root);
        textTimer.setText(value);
        textCounterT.setText(timeWork + "");
        textIter.setText("Всего " + totalIter + " подхода/-ов");
        workV.setText(timeWork + "");
        relaxV.setText(timeRelax + "");
        iteracV.setText(totalIter + "");
        return root;
    }

    boolean onStartTimer = false;
    CounterTimerProzess counterTimerProzess;

    @OnClick(R.id.workV)
    void onClickWork() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Время работы");
        builder.setMessage("Согласитесь или установите новое");
        final EditText input1 = new EditText(getActivity());
        input1.setText(timeWork + "");
        builder.setView(input1);

        builder.setPositiveButton("Правильно", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timeWork = Integer.parseInt(input1.getText().toString());
                timeCurreent = timeWork + timeRelax;
                textCounterT.setText(timeWork + "");
                workV.setText(timeWork + "");
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.relaxV)
    void onClickRelax() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Время отдыха");
        builder.setMessage("Согласитесь или установите новое");
        final EditText input = new EditText(getActivity());
        input.setText(timeRelax + "");
        builder.setView(input);

        builder.setPositiveButton("Правильно", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timeRelax = Integer.parseInt(input.getText().toString());
                timeCurreent = timeWork + timeRelax;
                relaxV.setText(timeRelax + "");
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @OnClick(R.id.iteracV)
    void onClickIterac() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Количество повторов");
        builder.setMessage("Согласитесь или установите новое");
        final EditText input = new EditText(getActivity());
        input.setText(totalIter + "");
        builder.setView(input);

        builder.setPositiveButton("Правильно", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                totalIter = Integer.parseInt(input.getText().toString());
                textIter.setText("Всего " + totalIter + " подхода/-ов");
                iteracV.setText(totalIter + "");
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private int onWork = 1; // 1 это true, 0 это false;
    private int currentIteraciy = 0;
    int timeCurreent = timeWork + timeRelax; // общее время цикла


    @OnClick(R.id.textCounterT)
    void onClickCounter() {

        if (!onStartTimer) {
            counterTimerProzess = new CounterTimerProzess();
            counterTimerProzess.execute(timeCurreent, currentIteraciy);
            onStartTimer = true;
        } else {
            onStartTimer = false;
            counterTimerProzess.cancel(true);
        }
    }

    @OnLongClick(R.id.textCounterT)
    boolean onLongClickCounter() {
        counterTimerProzess.cancel(true);
        onStartTimer = false;
        onWork = 1;
        timeCurreent = timeWork + timeRelax;
        currentIteraciy = 0;
        textCounterT.setText(timeWork + "");
        textIter.setText("Всего " + totalIter + " подхода/-ов");
        fon.setBackgroundColor(getResources().getColor(R.color.white));
        textWorkRelax.setText("");
        return true;
    }

    class CounterTimerProzess extends AsyncTask<Integer, Integer, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            int currentTimeL = params[0];
            int currentIteraciy = params[1];

            for (int i = currentIteraciy; i < totalIter; i++) {
                while (currentTimeL > 1) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (isCancelled()) break;
                    currentTimeL--;
                    if (onWork == 1) {
                        publishProgress(currentTimeL - timeRelax, i);
                        if (currentTimeL == timeRelax + 1) onWork = 0;
                    } else {
                        publishProgress(currentTimeL, i);

                    }

                }// конец while
                if (isCancelled()) break;
                onWork = 1;
                currentTimeL = timeWork + timeRelax + 1;

            }// iter
            if (isCancelled()) return null;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(0, totalIter);
            return null;

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            textCounterT.setText(values[0] + "");
            if (onWork == 1) {
                timeCurreent = values[0] + timeRelax;
                textWorkRelax.setText("РАБОТАЕМ!!!");
                fon.setBackgroundColor(getResources().getColor(R.color.red));

            } else {
                timeCurreent = values[0];
                textWorkRelax.setText("ОТДЫХАЕМ");
                fon.setBackgroundColor(getResources().getColor(R.color.blu));
            }
            currentIteraciy = values[1];

            textIter.setText(values[1] + 1 + " подход из " + totalIter);
            if (values[0] == 0) {
                textIter.setText("Окончено " + totalIter + " подхода");
                textWorkRelax.setText("");
                fon.setBackgroundColor(getResources().getColor(R.color.white));
            }
        }
    }
}