package com.ankir33gmail.stopwatchandtabbat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.viewTimer)
    void onTimerClick() {

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_left, R.anim.to_right)
                .replace(R.id.container, TimerFragment.newInstance("Таймер"))
                .commit();
    }

    @OnClick(R.id.viewStopWatch)
    void onStopWatchClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_right, R.anim.to_left)
                .replace(R.id.container, StopWatchFragment.newInstance("Секундомер"))
                .commit();
    }


}
